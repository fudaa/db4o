/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

/**
 * Class metadata to be stored to the database file
 * Don't obfuscate.
 * 
 */
public class MetaClass implements Internal4{
	
	public String name;
	public MetaField[] fields;
	
	public MetaClass(){
	}
	
	public MetaClass(String name){
		this.name = name;
	}
	
	MetaField ensureField(Transaction trans, String a_name){
		if(fields != null){
			for (int i = 0; i < fields.length; i++) {
				if(fields[i].name.equals(a_name)){
					return fields[i];
				}
            }
            MetaField[] temp = new MetaField[fields.length + 1];
            System.arraycopy(fields, 0, temp, 0, fields.length);
            fields = temp;
		}else{
			fields = new MetaField[1];
		}
		MetaField newMetaField = new MetaField(a_name);
		fields[fields.length - 1] = newMetaField;
		trans.i_stream.setInternal(trans, newMetaField, YapConst.UNSPECIFIED, false);
		trans.i_stream.setInternal(trans, this, YapConst.UNSPECIFIED, false);
		return newMetaField;
	}

}

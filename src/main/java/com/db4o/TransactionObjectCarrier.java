/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

/**
 * TODO: Check if all time-consuming stuff is overridden! 
 */
class TransactionObjectCarrier extends Transaction{
	
	TransactionObjectCarrier(YapStream a_stream, Transaction a_parent) {
		super(a_stream, a_parent);
	}
	
	void commit() {
		// do nothing
	}
	
    void slotFreeOnCommit(int a_id, int a_address, int a_length) {
//      do nothing
    }
    
    void slotFreeOnRollback(int a_id, int a_address, int a_length) {
//      do nothing
    }
    
    void slotFreeOnRollbackSetPointer(int a_id, int a_address, int a_length) {
        setPointer(a_id, a_address, a_length);
    }
    
    void slotFreeOnRollbackCommitSetPointer(int a_id, int newAddress, int newLength) {
        setPointer(a_id, newAddress, newLength);
    }
    
    void slotFreePointerOnCommit(int a_id, int a_address, int a_length) {
//      do nothing
    }
	
	void setPointer(int a_id, int a_address, int a_length) {
		writePointer(a_id, a_address, a_length);
	}
    
    boolean supportsVirtualFields(){
        return false;
    }
    
    
    

}

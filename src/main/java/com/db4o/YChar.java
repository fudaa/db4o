/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;


final class YChar extends YapJavaClass {

    static final int LENGTH = YapConst.CHAR_BYTES + YapConst.ADDED_LENGTH;
	
	private static final Character i_primitive = new Character((char)0);
	
    public YChar(YapStream stream) {
        super(stream);
    }
    
	public Object defaultValue(){
		return i_primitive;
	}
	
	public int getID() {
		return 7;
	}

	public int linkLength() {
		return LENGTH;
	}

	protected Class primitiveJavaClass() {
		return char.class;
	}

	Object primitiveNull() {
		return i_primitive;
	}

	Object read1(YapReader a_bytes) {
		if (Deploy.debug) {
			a_bytes.readBegin(YapConst.YAPCHAR);
		}
		byte b1 = a_bytes.readByte();
		byte b2 = a_bytes.readByte();
		if (Deploy.debug) {
			a_bytes.readEnd();
		}
		char ret = (char) ((b1 & 0xff) | ((b2 & 0xff) << 8));
		if(! Deploy.csharp){
			if (ret == Character.MAX_VALUE) {
				return null;
			}
		}
		return new Character(ret);
	}

	public void write(Object a_object, YapWriter a_bytes) {
		if (Deploy.debug) {
			a_bytes.writeBegin(YapConst.YAPCHAR);
		}
		char l_char;
		if (! Deploy.csharp && a_object == null) {
			l_char = Character.MAX_VALUE;
		} else {
			l_char = ((Character) a_object).charValue();
		}
		a_bytes.append((byte) (l_char & 0xff));
		a_bytes.append((byte) (l_char >> 8));
		if (Deploy.debug) {
			a_bytes.writeEnd();
		}
	}

	// Comparison_______________________

	private char i_compareTo;

	private char val(Object obj) {
		return ((Character) obj).charValue();
	}

	void prepareComparison1(Object obj) {
		i_compareTo = val(obj);
	}

	boolean isEqual1(Object obj) {
		return obj instanceof Character && val(obj) == i_compareTo;
	}

	boolean isGreater1(Object obj) {
		return obj instanceof Character && val(obj) > i_compareTo;
	}

	boolean isSmaller1(Object obj) {
		return obj instanceof Character && val(obj) < i_compareTo;
	}

}

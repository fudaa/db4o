/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import java.io.*;

import com.db4o.ext.*;
import com.db4o.foundation.network.*;


class MReadBlob extends MsgBlob {

    void processClient(YapSocket sock) {
        Msg message = Msg.readMessage(getTransaction(), sock);
        if (message.equals(Msg.LENGTH)) {
            try {
                i_currentByte = 0;
                i_length = message.getPayLoad().readInt();
                i_blob.getStatusFrom(this);
                i_blob.setStatus(Status.PROCESSING);
                copy(sock,this.i_blob.getClientOutputStream(),i_length,true);
                message = Msg.readMessage(getTransaction(), sock);
                if (message.equals(Msg.OK)) {
                    this.i_blob.setStatus(Status.COMPLETED);
                } else {
                    this.i_blob.setStatus(Status.ERROR);
                }
            } catch (Exception e) {
            }
        } else if (message.equals(Msg.ERROR)) {
            this.i_blob.setStatus(Status.ERROR);
        }

    }
    boolean processMessageAtServer(YapSocket sock) {
        YapStream stream = getStream();
        try {
            BlobImpl blobImpl = this.serverGetBlobImpl();
            if (blobImpl != null) {
                blobImpl.setTrans(getTransaction());
                File file = blobImpl.serverFile(null, false);
                int length = (int) file.length();
                Msg.LENGTH.getWriterForInt(getTransaction(), length).write(stream, sock);
                FileInputStream fin = new FileInputStream(file);
                copy(fin,sock,false);
                sock.flush();
                Msg.OK.write(stream, sock);
            }
        } catch (Exception e) {
            Msg.ERROR.write(stream, sock);
        }
        return true;
    }
}
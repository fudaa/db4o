/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.config.*;

/**
 */
public class Unobfuscated {
    
    static Object random;
    
	static boolean createDb4oList(Object a_stream){
	    ((YapStream)a_stream).checkClosed();
	    return ! ((YapStream)a_stream).isInstantiating();
	}
	
	public static byte[] generateSignature() {
	    // TODO: We could add part of the file name to improve 
	    //       signature security.
	    YapWriter writer = new YapWriter(null, 300);
	    if(! Deploy.csharp) {
		    try {
	            new YapStringIO().write(writer, java.net.InetAddress.getLocalHost().getHostName());
	            writer.append((byte)0);
	            writer.append(java.net.InetAddress.getLocalHost().getAddress());
	        } catch (Exception e) {
	        }
	    }
	    YLong.writeLong(System.currentTimeMillis(), writer);
        YLong.writeLong(randomLong(), writer);
        YLong.writeLong(randomLong() + 1, writer);
        return writer.getWrittenBytes();
	}
	
	static void logErr (Configuration config, int code, String msg, Throwable t) {
		Messages.logErr(config, code, msg, t);
	}
	
	static void purgeUnsychronized(Object a_stream, Object a_object){
	    ((YapStream)a_stream).purge1(a_object);
	}
	
	public static long randomLong() {
	    if(Deploy.csharp) {
	        // TODO: route to .NET implementation
	        return System.currentTimeMillis();
	    }else {
	        if(random == null){
	            random = new java.util.Random();
	        }
	        return ((java.util.Random)random).nextLong();
	    }
	}
	
	static void shutDownHookCallback(Object a_stream){
		((YapStream)a_stream).failedToShutDown();
	}


}

/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.Coercion4;
import com.db4o.reflect.*;


final class YFloat extends YInt {
    
    private static final Float i_primitive = new Float(0);
    
    public YFloat(YapStream stream) {
        super(stream);
    }
    
    public Object coerce(ReflectClass claxx, Object obj) {
    	return Coercion4.toFloat(obj);
    }

	public Object defaultValue(){
		return i_primitive;
	}
	
	public int getID() {
		return 3;
	}

	protected Class primitiveJavaClass() {
		return float.class;
	}

	Object primitiveNull() {
		return i_primitive;
	}

	Object read1(YapReader a_bytes) {
		int ret = readInt(a_bytes);
		if(! Deploy.csharp){
			if (ret == Integer.MAX_VALUE) {
				return null;
			}
		}
		return new Float(Float.intBitsToFloat(ret));
	}

	public void write(Object a_object, YapWriter a_bytes) {
		if (! Deploy.csharp && a_object == null) {
			writeInt(Integer.MAX_VALUE, a_bytes);
		} else {
			writeInt(
				Float.floatToIntBits(((Float) a_object).floatValue()),
				a_bytes);
		}
	}

	// Comparison_______________________

	private float i_compareTo;

	private float valu(Object obj) {
		return ((Float) obj).floatValue();
	}

	void prepareComparison1(Object obj) {
		i_compareTo = valu(obj);
	}

	boolean isEqual1(Object obj) {
		return obj instanceof Float && valu(obj) == i_compareTo;
	}

	boolean isGreater1(Object obj) {
		return obj instanceof Float && valu(obj) > i_compareTo;
	}

	boolean isSmaller1(Object obj) {
		return obj instanceof Float && valu(obj) < i_compareTo;
	}

}

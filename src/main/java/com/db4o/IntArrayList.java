/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

/**
 */
public class IntArrayList {
    
    static final int INC = 20;
    
    protected int[] i_content;
    
    private int i_current;
    private int i_count;
    
    public IntArrayList(){
        this(INC);
    }
    
    public IntArrayList(int initialSize){
        i_content = new int[initialSize];
    }
    
    public void add(int a_value){
        if(i_count >= i_content.length){
            int[] temp = new int[i_content.length + INC];
            System.arraycopy(i_content, 0, temp, 0, i_content.length);
            i_content = temp;
        }
        i_content[i_count++] = a_value;
    }
    
    public int indexOf(int a_value) {
        for (int i = 0; i < i_count; i++) {
            if (i_content[i] == a_value){
                return i;
            }
        }
        return -1;
    }
    
    public int size(){
        return i_count;
    }
    
    public void reset() {
        i_current = i_count - 1;
    }
    
    public boolean hasNext(){
        return i_current >= 0;
    }
    
    public int nextInt(){
        return i_content[i_current --];
    }
    
    public long[] asLong(){
        long[] longs = new long[i_count];
        for (int i = 0; i < i_count; i++) {
            longs[i] = i_content[i]; 
        }
        return longs;
    }

}

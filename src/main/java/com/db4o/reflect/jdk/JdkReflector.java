/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.reflect.jdk;

import com.db4o.reflect.*;

public class JdkReflector implements Reflector{
	
    private final ClassLoader _classLoader;
    private Reflector _parent;
    private ReflectArray _array;
    
	public JdkReflector(ClassLoader classLoader){
		_classLoader = classLoader;
	}
	
	public ReflectArray array(){
        if(_array == null){
            _array = new JdkArray(_parent);
        }
		return _array;
	}
	
	public boolean constructorCallsSupported(){
		return true;
	}
    
    public Object deepClone(Object obj) {
        return new JdkReflector(_classLoader);
    }
	
	public ReflectClass forClass(Class clazz){
        return new JdkClass(_parent, clazz);
	}
	
	public ReflectClass forName(String className) {
		try {
            Class clazz = _classLoader == null ? Class.forName(className) : _classLoader.loadClass(className);
            if(clazz == null){
                return null;
            }
            return new JdkClass(_parent, clazz);
		}
		catch(Exception exc) {
		}
		return null;
	}
	
	public ReflectClass forObject(Object a_object) {
		if(a_object == null){
			return null;
		}
		return _parent.forClass(a_object.getClass());
	}
	
	public boolean isCollection(ReflectClass candidate) {
		return false;
	}

	public boolean methodCallsSupported(){
		return true;
	}

    public void setParent(Reflector reflector) {
        _parent = reflector;
    }

    public static ReflectClass[] toMeta(Reflector reflector, Class[] clazz){
        ReflectClass[] claxx = null;
        if(clazz != null){
            claxx = new ReflectClass[clazz.length];
            for (int i = 0; i < clazz.length; i++) {
                if(clazz[i] != null){
                    claxx[i] = reflector.forClass(clazz[i]);
                }
            }
        }
        return claxx;
    }
    
    static Class[] toNative(ReflectClass[] claxx){
        Class[] clazz = null;
        if(claxx != null){
            clazz = new Class[claxx.length];
            for (int i = 0; i < claxx.length; i++) {
                if(claxx[i] != null){
                    clazz[i] = ((JdkClass)claxx[i].getDelegate()).getJavaClass();
                }
            }
        }
        return clazz;
    }

}

/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

/**
 * client class index. Largly intended to do nothing or
 * redirect functionality to the server.
 */
final class ClassIndexClient extends ClassIndex {

	ClassIndexClient(YapClass aYapClass) {
		super(aYapClass);
	}

	void add(int a_id){
		throw YapConst.virtualException();
	}
    
    void ensureActive(){
        // do nothing
    }
	
	long[] getInternalIDs(Transaction trans, int yapClassID){
		YapClient stream = (YapClient)trans.i_stream;
		stream.writeMsg(Msg.GET_INTERNAL_IDS.getWriterForInt(trans, yapClassID));
		YapWriter reader = stream.expectedByteResponse(Msg.ID_LIST);
		int size = reader.readInt();
		long[] ids = new long[size];
		for (int i = 0; i < size; i++) {
			ids[i] = reader.readInt();
		}
		return ids;
	}
	
	void read(Transaction a_trans) {
		// do nothing
	}

	void setDirty(YapStream a_stream) {
		// do nothing
	}

	void setID(YapStream a_stream, int a_id) {
		// do nothing and dont cache
		// ID will remain zero, so the index
		// will be stored automatically on the server side
	}

	void write(YapStream a_stream) {
		// do nothing
	}

	final void writeOwnID(YapWriter a_writer) {
		a_writer.writeInt(0);
	}
	

}
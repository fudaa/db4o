/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

/**
 */
public abstract class QEStringCmp extends QEAbstract {
	private boolean caseSensitive;

	public QEStringCmp(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}

	boolean evaluate(QConObject a_constraint, QCandidate a_candidate, Object a_value){
		if(a_value != null){
		    if(a_value instanceof YapReader) {
		        a_value = ((YapReader)a_value).toString(a_constraint.i_trans);
		    }
		    String candidate=a_value.toString();
		    String constraint=a_constraint.i_object.toString();
		    if(!caseSensitive) {
		    	candidate=candidate.toLowerCase();
		    	constraint=constraint.toLowerCase();
		    }
			return compareStrings(candidate,constraint);
		}
		return a_constraint.i_object.equals(null);
	}
	
	public boolean supportsIndex(){
	    return false;
	}
	
	protected abstract boolean compareStrings(String candidate,String constraint);
}

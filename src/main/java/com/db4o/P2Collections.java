/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.types.*;

/**
 * @persistent
 */
class P2Collections implements Db4oCollections{
    
    final YapStream i_stream;
    
    P2Collections(YapStream a_stream){
        i_stream = a_stream;
    }

    public Db4oList newLinkedList() {
        synchronized(i_stream.i_lock) {
	        if(Unobfuscated.createDb4oList(i_stream)){
	            Db4oList l = new P2LinkedList();
	            i_stream.set(l);
	            return l;
	        }
	        return null;
        }
    }

    public Db4oMap newHashMap(int a_size) {
        synchronized(i_stream.i_lock) {
	        if(Unobfuscated.createDb4oList(i_stream)){
	            return new P2HashMap(a_size);
	        }
	        return null;
        }
    }
    
    public Db4oMap newIdentityHashMap(int a_size) {
        synchronized(i_stream.i_lock) {
	        if(Unobfuscated.createDb4oList(i_stream)){
	            P2HashMap m = new P2HashMap(a_size);
	            m.i_type = 1;
	            i_stream.set(m);
	            return m;
	        }
	        return null;
        }
    }

}

/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.io;

import java.io.*;

import com.db4o.*;

/**
 * IO adapter for random access files.
 */
public class RandomAccessFileAdapter extends IoAdapter {

    private RandomAccessFile _delegate;

    public RandomAccessFileAdapter(){
    }

    protected RandomAccessFileAdapter(String path, boolean lockFile, long initialLength) throws IOException {
        _delegate = new RandomAccessFile(path, "rw");
        if(initialLength>0) {
	        _delegate.seek(initialLength - 1);
	        _delegate.write(new byte[] {0});
        }
        if(lockFile){
            Platform4.lock(_delegate);
        }
    }

    public void close() throws IOException {
        try {
            Platform4.unlock(_delegate);
        } catch (Exception e) {
        }
        _delegate.close();
    }

    public long getLength() throws IOException {
        return _delegate.length();
    }

    public IoAdapter open(String path, boolean lockFile, long initialLength) throws IOException {
        return new RandomAccessFileAdapter(path, lockFile, initialLength);
    }

    public int read(byte[] bytes, int length) throws IOException {
        return _delegate.read(bytes, 0, length);
    }

    public void seek(long pos) throws IOException {

        if(DTrace.enabled){
            DTrace.REGULAR_SEEK.log(pos);
        }
        _delegate.seek(pos);

    }

    public void sync() throws IOException {
        _delegate.getFD().sync();
    }

    public void write(byte[] buffer, int length) throws IOException {
        _delegate.write(buffer, 0, length);
    }
}

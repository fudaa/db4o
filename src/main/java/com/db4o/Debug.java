/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.*;

/**
 */
public abstract class Debug extends Debug4 {
    
    public static final boolean useNIxPaths = true;
    
    public static final boolean ixTrees = false;
    
    public static final boolean freespace = Deploy.debug ? true :false;
    
    public static final boolean xbytes = Debug.freespace ? true : false;
    
    public static final boolean freespaceChecker = false;
    
    public static final boolean checkSychronization = false;
    
    public static final boolean atHome = false;

    public static final boolean indexAllFields = false;
    
    public static final boolean configureAllClasses = indexAllFields;
    public static final boolean configureAllFields = indexAllFields;
    
    public static final boolean weakReferences = true;

    public static final boolean arrayTypes = true;

    public static final boolean verbose = false;

    public static final boolean fakeServer = false;
    static final boolean messages = false;

    public static final boolean nio = true;
    
    static final boolean lockFile = true;

    static final boolean longTimeOuts = false;

    static YapFile serverStream;
    static YapClient clientStream;
    static Queue4 clientMessageQueue;
    static Lock4 clientMessageQueueLock;
    
    public static void expect(boolean cond){
        if(! cond){
            throw new RuntimeException("Should never happen");
        }
    }
    
    public static void ensureLock(Object obj) {
        if (atHome) {
            try {
                obj.wait(1);
            } catch (IllegalMonitorStateException imse) {
                System.err.println("No Lock Alarm.");
                imse.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean exceedsMaximumBlockSize(int a_length) {
        if (a_length > YapConst.MAXIMUM_BLOCK_SIZE) {
            if (atHome) {
                System.err.println("Maximum block size  exceeded!!!");
                new Exception().printStackTrace();
            }
            return true;
        }
        return false;
    }
    
    public static boolean exceedsMaximumArrayEntries(int a_entries, boolean a_primitive){
        if (a_entries > (a_primitive ? YapConst.MAXIMUM_ARRAY_ENTRIES_PRIMITIVE : YapConst.MAXIMUM_ARRAY_ENTRIES)) {
            if (atHome) {
                System.err.println("Maximum array elements exceeded!!!");
                new Exception().printStackTrace();
            }
            return true;
        }
        return false;
    }

}

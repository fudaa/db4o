/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.foundation.network;

import java.io.*;
import java.net.*;

public class YapSocketReal implements YapSocket {

    private Socket _socket;
    private OutputStream _out;
    private InputStream _in;
    
    public YapSocketReal(Socket socket) throws IOException {
    	_socket = socket;
    	_out = _socket.getOutputStream();
    	_in = _socket.getInputStream();
    }

    public void close() throws IOException {
        //    	i_socket.getInputStream().close();
        //    	i_socket.getOutputStream().close();
        _socket.close();
    }

    public void flush() throws IOException {
        _out.flush();
    }
    
    public int read() throws IOException {
        return _in.read();
    }

    public int read(byte[] a_bytes, int a_offset, int a_length) throws IOException {
        return _in.read(a_bytes, a_offset, a_length);
    }

    public void setSoTimeout(int timeout) {
        try {
            _socket.setSoTimeout(timeout);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

	public void write(byte[] bytes) throws IOException {
	    _out.write(bytes);
	}

    public void write(byte[] bytes,int off,int len) throws IOException {
        _out.write(bytes,off,len);
    }

    public void write(int i) throws IOException {
        _out.write(i);
    }
    
	public YapSocket openParalellSocket() throws IOException {
		throw new IOException(); // not supported
	}
}

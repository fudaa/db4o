/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.foundation.network;

import java.io.*;

/**
 * Fakes a socket connection for an embedded client.
 */
public class YapSocketFake implements YapSocket {
	
	private final YapSocketFakeServer _server;

    private YapSocketFake _affiliate;
    private ByteBuffer4 _uploadBuffer;
    private ByteBuffer4 _downloadBuffer;

    public YapSocketFake(YapSocketFakeServer a_server, int timeout) {
    	_server = a_server;
        _uploadBuffer = new ByteBuffer4(timeout);
        _downloadBuffer = new ByteBuffer4(timeout);
    }

    public YapSocketFake(YapSocketFakeServer a_server, int timeout, YapSocketFake affiliate) {
        this(a_server, timeout);
        _affiliate = affiliate;
        affiliate._affiliate = this;
        _downloadBuffer = affiliate._uploadBuffer;
        _uploadBuffer = affiliate._downloadBuffer;
    }

    public void close() throws IOException {
        if (_affiliate != null) {
            YapSocketFake temp = _affiliate;
            _affiliate = null;
            temp.close();
        }
        _affiliate = null;
    }

    public void flush() {
        // do nothing
    }

    public boolean isClosed() {
        return _affiliate == null;
    }

    public int read() throws IOException {
        return _downloadBuffer.read();
    }

    public int read(byte[] a_bytes, int a_offset, int a_length) throws IOException {
        return _downloadBuffer.read(a_bytes, a_offset, a_length);
    }

    public void setSoTimeout(int a_timeout) {
        _uploadBuffer.setTimeout(a_timeout);
        _downloadBuffer.setTimeout(a_timeout);
    }

    public void write(byte[] bytes) throws IOException {
        _uploadBuffer.write(bytes);
    }
    
    public void write(byte[] bytes,int off,int len) throws IOException {
        _uploadBuffer.write(bytes, off, len);
    }

    public void write(int i) throws IOException {
        _uploadBuffer.write(i);
    }
    
    public YapSocket openParalellSocket() throws IOException {
    	return _server.openClientSocket();
    }
}

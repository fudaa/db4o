/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;


final class YBoolean extends YapJavaClass
{

    static final int LENGTH = 1 + YapConst.ADDED_LENGTH;
	
	private static final byte TRUE = (byte) 'T';
	private static final byte FALSE = (byte) 'F';
	private static final byte NULL = (byte) 'N';
	
	private static final Boolean i_primitive = new Boolean(false);
	
    public YBoolean(YapStream stream) {
        super(stream);
    }
    
	public int getID(){
		return 4;
	}
	
	public Object defaultValue(){
		return i_primitive;
	}
	
	public int linkLength(){
		return LENGTH;
	}
	
	protected Class primitiveJavaClass(){
		return boolean.class;
	}
	
	Object primitiveNull(){
		return i_primitive;
	}

	Object read1(YapReader a_bytes){
		if (Deploy.debug){
			a_bytes.readBegin(YapConst.YAPBOOLEAN);
		}
		byte ret = a_bytes.readByte();
		if (Deploy.debug){
			a_bytes.readEnd();
		}
		
		if(ret == TRUE){
			return new Boolean(true);
		}
		if(ret == FALSE){
			return new Boolean(false);
		}
		
		return null;
	}
	
	public void write(Object a_object, YapWriter a_bytes){
		if(Deploy.debug){
			a_bytes.writeBegin(YapConst.YAPBOOLEAN);
		}
		byte set;
		if (a_object == null){
			set = NULL;
		} else {
			if(((Boolean)a_object).booleanValue()){
				set = TRUE;
			}else{
				set = FALSE;
			}
		}
		a_bytes.append(set);
		if(Deploy.debug){
			a_bytes.writeEnd();
		}
	}

	
	// Comparison_______________________
	
	private boolean i_compareTo;
	
	private boolean val(Object obj){
		return ((Boolean)obj).booleanValue();
	}
	
	void prepareComparison1(Object obj){
		i_compareTo = val(obj);
	}
	
	boolean isEqual1(Object obj){
		return obj instanceof Boolean && val(obj) == i_compareTo;
	}
	
	boolean isGreater1(Object obj){
	    if(i_compareTo){
	        return false;
	    }
		return obj instanceof Boolean && val(obj);
	}
	
	boolean isSmaller1(Object obj){
	    if(! i_compareTo){
	        return false;
	    }
	    return obj instanceof Boolean && ! val(obj);
	}
}

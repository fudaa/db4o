/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.network.*;
import com.db4o.reflect.*;

final class MCreateClass extends MsgD {
    final boolean processMessageAtServer(YapSocket sock) {
        YapStream stream = getStream();
        Transaction trans = stream.getSystemTransaction();
        YapWriter returnBytes = new YapWriter(trans, 0);
        try{
            ReflectClass claxx = trans.reflector().forName(readString());
            if (claxx != null) {
                synchronized (stream.i_lock) {
                    try {
                        YapClass yapClass = stream.getYapClass(claxx, true);
                        if (yapClass != null) {
                            stream.checkStillToSet();
                            yapClass.setStateDirty();
                            yapClass.write(trans);
                            trans.commit();
                            returnBytes = stream.readWriterByID(trans, yapClass.getID());
                            Msg.OBJECT_TO_CLIENT.getWriter(returnBytes).write(stream, sock);
                            return true;
    
                        } else {
                            // TODO: handling, if the class can't be created 
                        }
                    } catch (Throwable t) {
                        if (Deploy.debug) {
                            System.out.println("MCreateClass failed");
                        }
                    }
                }
            }
        }catch(Throwable th){
            if (Deploy.debug) {
                System.out.println("MCreateClass failed");
            }
        }
        Msg.FAILED.write(stream, sock);
        return true;
    }
}

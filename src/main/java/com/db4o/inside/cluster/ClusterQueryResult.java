/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.cluster;

import com.db4o.*;
import com.db4o.cluster.*;
import com.db4o.inside.*;
import com.db4o.inside.query.*;
import com.db4o.query.*;

/**
 * 
 */
public class ClusterQueryResult implements QueryResult{
    
    private final Cluster _cluster;
    private final ObjectSet[] _objectSets;
    private int _current;
    private final int[] _sizes;
    private final int _size;
    
    public ClusterQueryResult(Cluster cluster, Query[] queries){
        _cluster = cluster;
        _objectSets = new ObjectSet[queries.length]; 
        _sizes = new int[queries.length];
        int size = 0;
        for (int i = 0; i < queries.length; i++) {
            _objectSets[i] = queries[i].execute();
            _sizes[i] = _objectSets[i].size();
            size += _sizes[i];
        }
        _size = size;
    }

    public boolean hasNext() {
        synchronized(_cluster){
            return hasNextNoSync();
        }
    }
    
    private ObjectSet current(){
        return _objectSets[_current];
    }
    
    private boolean hasNextNoSync(){
        if(current().hasNext()){
            return true;
        }
        if(_current >= _objectSets.length-1){
            return false;
        }
        _current ++;
        return hasNextNoSync();
    }

    public Object next() {
        synchronized(_cluster){
            if(hasNextNoSync()){
                return current().next();
            }
            return null;
        }
    }

    public void reset() {
        synchronized(_cluster){
            for (int i = 0; i < _objectSets.length; i++) {
               _objectSets[i].reset(); 
            }
            _current = 0;
        }
    }

    public int size() {
        return _size;
    }
    
    public Object get(int index) {
        synchronized(_cluster){
            if (index < 0 || index >= size()) {
                throw new IndexOutOfBoundsException();
            }
            int i = 0;
            while(index >= _sizes[i]){
                index -= _sizes[i];
                i++;
            }
            return ((ObjectSetFacade)_objectSets[i])._delegate.get(index); 
        }
    }

    public long[] getIDs() {
        Exceptions4.notSupported();
        return null;
    }

    public Object streamLock() {
        return _cluster;
    }

    public ObjectContainer objectContainer() {
        return _cluster._objectContainers[_current];
    }

    public int indexOf(int id) {
        Exceptions4.notSupported();
        return 0;
    }

	public void sort(QueryComparator cmp) {
        Exceptions4.notSupported();
	}
}


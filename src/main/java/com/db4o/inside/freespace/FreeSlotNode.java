/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.freespace;

import com.db4o.*;

/**
 */
public final class FreeSlotNode extends TreeInt
{
    public static int sizeLimit;
    
	FreeSlotNode i_peer;
	
	FreeSlotNode(int a_key) {
        super(a_key);
    }
	
	final void createPeer(int a_key){
		i_peer = new FreeSlotNode(a_key);
		i_peer.i_peer = this;
	}
	
	public boolean duplicates(){
		return true;
	}
	
	public final int ownLength(){
		return YapConst.YAPINT_LENGTH * 2;
	}
	
	final static Tree removeGreaterOrEqual(FreeSlotNode a_in, TreeIntObject a_finder){
		if(a_in == null){
			return null;
		}
		int cmp = a_in.i_key - a_finder.i_key;
		if(cmp == 0){
			a_finder.i_object = a_in; // the highest node in the hierarchy !!!
			return a_in.remove(); 
		}else{
			if(cmp > 0){
				a_in.i_preceding = removeGreaterOrEqual((FreeSlotNode)a_in.i_preceding, a_finder);
				if(a_finder.i_object != null){
					a_in.i_size --;
					return a_in;
				}
				a_finder.i_object = a_in;
				return a_in.remove(); 
			}else{
				a_in.i_subsequent = removeGreaterOrEqual((FreeSlotNode)a_in.i_subsequent, a_finder);
				if(a_finder.i_object != null){
				    a_in.i_size --;
				}
				return a_in;
			}
		}
	}


	public Object read(YapReader a_reader){
	    int size = a_reader.readInt();
	    int address = a_reader.readInt();
	    if(size > sizeLimit){
	        FreeSlotNode node = new FreeSlotNode(size);
	        node.createPeer(address);
	        if(Deploy.debug){
	            if(a_reader instanceof YapWriter){
	                Transaction trans = ((YapWriter)a_reader).getTransaction();
	                if(trans.i_stream instanceof YapRandomAccessFile){
		                YapWriter checker = trans.i_stream.getWriter(trans, node.i_peer.i_key, node.i_key);
		                checker.read();
		                for (int i = 0; i < node.i_key; i++){
		                    if(checker.readByte() != (byte)'X'){
		                        System.out.println("!!! Free space corruption at:" + node.i_peer.i_key);
		                        break;
		                    }
		                }
	                }
	            }
	        }
	        return node;
	    }
	    return null;
	}

	public final void write(YapWriter a_writer){
		// byte order: size, address
		a_writer.writeInt(i_key);
		a_writer.writeInt(i_peer.i_key);
	}
	
	
//	public static final void debug(FreeSlotNode a_node){
//		if(a_node == null){
//			return;
//		}
//		System.out.println("Address:" + a_node.i_key);
//		System.out.println("Length:" + a_node.i_peer.i_key);
//		debug((FreeSlotNode)a_node.i_preceding);
//		debug((FreeSlotNode)a_node.i_subsequent);
//	}
    
    public String toString() {
        if(!Debug.freespace){
            return super.toString();
            
        }
        String str = "FreeSlotNode " + i_key;
        if(i_peer != null){
            str += " peer: " + i_peer.i_key;
        }
        return str;
    }
}

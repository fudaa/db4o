/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.freespace;

import com.db4o.*;
import com.db4o.foundation.*;
import com.db4o.inside.ix.*;


abstract class FreespaceIx {
    
    Index4 _index;
    
    IndexTransaction _indexTrans;
    
    IxTraverser _traverser;
    
    FreespaceVisitor _visitor;
    
    FreespaceIx(YapFile file, MetaIndex metaIndex){
        _index = new Index4(file.getSystemTransaction(),new YInt(file), metaIndex, false);
        _indexTrans = _index.globalIndexTransaction();
    }
    
    abstract void add(int address, int length);
    
    abstract int address();
    
    public void debug(){
        if(Debug.freespace){
            IxTree tree = (IxTree) _indexTrans.getRoot();
            if(tree != null){
                tree.traverse(new Visitor4(){
                    public void visit(Object obj) {
                        System.out.println(obj);
                    }
                });
            }
        }
    }
    
    void find (int val){
        _traverser = new IxTraverser();
        _traverser.findBoundsExactMatch(new Integer(val), (IxTree)_indexTrans.getRoot());
    }
    
    abstract int length();
    
    boolean match(){
        _visitor = new FreespaceVisitor();
        _traverser.visitMatch(_visitor);
        return _visitor.visited();
    }
    
    boolean preceding(){
        _visitor = new FreespaceVisitor();
        _traverser.visitPreceding(_visitor);
        return _visitor.visited();
    }
    
    abstract void remove(int address, int length);
    
    boolean subsequent(){
        _visitor = new FreespaceVisitor();
        _traverser.visitSubsequent(_visitor);
        return _visitor.visited();
    }

}

/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.ix;

import com.db4o.*;
import com.db4o.foundation.*;

/**
 */
public class QxProcessor {
    
    private Tree _paths;
    
    private QxPath _best;
    
    private int _limit;

    void addPath(final QxPath newPath){
        
        if(_paths == null){
            _paths = newPath;
            return;
        }
        
        if(! Debug.useNIxPaths){
            _paths = Tree.add(_paths, newPath);
            return;
        }
        
        final QxPath[] same = new QxPath[] {null};
        
        _paths.traverse(new Visitor4() {
            public void visit(Object a_object) {
                QxPath path = (QxPath)a_object;
                if(path._parent == newPath._parent){
                    if(path.onSameFieldAs(newPath)){
                        same[0] = path;
                    }
                }
            }
        });
        
        if(same[0] != null){
            _paths = _paths.removeNode(same[0]);
            newPath.mergeForSameField(same[0]);
        }
        
        _paths = Tree.add(_paths, newPath);
    }
    
    private void buildPaths(QCandidates candidates){
        Iterator4 i = candidates.iterateConstraints();
        while(i.hasNext()){
            QCon qCon = (QCon)i.next();
            qCon.setCandidates(candidates);
            if(! qCon.hasOrJoins()){
                new QxPath(this, null, qCon, 0).buildPaths();
            }
        }
    }

    public boolean run(QCandidates candidates, int limit){
        _limit = limit;
        buildPaths(candidates);
        if(_paths == null){
            return false;
        }
        return chooseBestPath();
    }
    
    private boolean chooseBestPath(){
        while(_paths != null){
            QxPath path = (QxPath)_paths.first();
            _paths = _paths.removeFirst();
            if(path.isTopLevelComplete()){
                _best = path;
                return true;
            }
            path.load();
        }
        return false;
    }
    
    public Tree toQCandidates(QCandidates candidates){
        return _best.toQCandidates(candidates);
    }
    
    boolean exceedsLimit(int count, int depth){
        int limit = _limit;
        for (int i = 0; i < depth; i++) {
            limit = limit / 10;
        }
        return count > limit;
    }

}

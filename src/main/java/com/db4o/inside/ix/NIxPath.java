/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.ix;

import com.db4o.*;
import com.db4o.foundation.*;

/**
 */
public class NIxPath extends Tree {
    
    // for debugging purposes, turn on in IxTraverser also
    // public Object _constraint;
    
    final NIxPathNode _head;
    
    final boolean _takePreceding;
    
    final boolean _takeMatches; 
    
    final boolean _takeSubsequent;
    
    int _type;  // see constants in IxTraverser
    
    public NIxPath(NIxPathNode head, boolean takePreceding, boolean takeMatches, boolean takeSubsequent, int pathType){
        _head = head;
        _takePreceding = takePreceding;
        _takeMatches = takeMatches;
        _takeSubsequent = takeSubsequent;
        _type = pathType;
    }
    
    public int compare(Tree a_to) {
        NIxPath other = (NIxPath)a_to;
        return _head.compare(other._head, _type, other._type);
    }
    
    public String toString() {
        if(! Debug4.prettyToStrings){
            return super.toString();
        }
        String str = "NIxPath +\n";
        String space = " ";
        NIxPathNode node = _head;
        while(node != null){
            str += space;
            space += " ";
            str += node.toString() + "\n";
            node = node._next;
        }
        return str;
    }

}

/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.ix;

import com.db4o.*;
import com.db4o.foundation.*;
import com.db4o.inside.freespace.*;

/**
 * An addition to a field index.
 */
public class IxAdd extends IxPatch {
    
    boolean i_keepRemoved;

    public IxAdd(IndexTransaction a_ft, int a_parentID, Object a_value) {
        super(a_ft, a_parentID, a_value);
    }
    
    void beginMerge(){
        super.beginMerge();
        handler().prepareComparison( handler().comparableObject(trans(), i_value));
    }
    
    public void visit(Object obj){
        ((Visitor4)obj).visit(new Integer(i_parentID));
    }
    
    public void visit(Visitor4 visitor, int[] lowerAndUpperMatch){
        visitor.visit(new Integer(i_parentID));
    }
    
    public void freespaceVisit(FreespaceVisitor visitor, int index){
        visitor.visit(i_parentID, ((Integer)i_value).intValue());
    }
    
    public int write(Indexable4 a_handler, YapWriter a_writer) {
        a_handler.writeIndexEntry(a_writer, i_value);
        a_writer.writeInt(i_parentID);
        a_writer.writeForward();
        return 1;
    }
    
    public String toString(){
        if(! Debug4.prettyToStrings){
            return super.toString();
        }
        String str = "IxAdd "  + i_parentID + "\n " + handler().comparableObject(trans(), i_value);
        return str;
    }

    public void visitAll(IntObjectVisitor visitor) {
        visitor.visit(i_parentID, handler().comparableObject(trans(), i_value));
    }

}

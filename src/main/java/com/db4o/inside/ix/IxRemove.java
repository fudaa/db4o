/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.ix;

import com.db4o.*;
import com.db4o.foundation.*;
import com.db4o.inside.freespace.*;

/**
 * A node to represent an entry removed from an Index
 */
public class IxRemove extends IxPatch {

    public IxRemove(IndexTransaction a_ft, int a_parentID, Object a_value) {
        super(a_ft, a_parentID, a_value);
        i_size = 0;
    }
    
    public int ownSize() {
        return 0;
    }

    public String toString() {
        if(! Debug4.prettyToStrings){
            return super.toString();
        }
        String str = "IxRemove " + i_parentID + "\n " + handler().comparableObject(trans(), i_value);
        return str;
    }
    
    public void freespaceVisit(FreespaceVisitor visitor, int index){
        // do nothing
    }
    
    public void visit(Object obj){
        // do nothing
    }
    
    public void visit(Visitor4 visitor, int[] lowerAndUpperMatch){
        // do nothing
    }

    public int write(Indexable4 a_handler, YapWriter a_writer) {
        // do nothing
        return 0;
    }

    public void visitAll(IntObjectVisitor visitor) {
        // do nothing
    }
}

/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o.inside.ix;

import com.db4o.*;
import com.db4o.foundation.*;
import com.db4o.inside.freespace.*;

/**
 */
public abstract class IxTree extends Tree implements Visitor4{
    
    IndexTransaction i_fieldTransaction;
    
    int i_version;
    
    int _nodes = 1;
    
    IxTree(IndexTransaction a_ft){
        i_fieldTransaction = a_ft;
        i_version = a_ft.i_version;
    }
    
    public Tree add(final Tree a_new, final int a_cmp){
        if(a_cmp < 0){
            if(i_subsequent == null){
                i_subsequent = a_new;
            }else{
                i_subsequent = i_subsequent.add(a_new);
            }
        }else {
            if(i_preceding == null){
                i_preceding = a_new;
            }else{
                i_preceding = i_preceding.add(a_new);
            }
        }
        return balanceCheckNulls();
    }
    
    void beginMerge(){
        i_preceding = null;
        i_subsequent = null;
        setSizeOwn();
    }
    
    public Tree deepClone(Object a_param){
        try {
            IxTree tree = (IxTree)this.clone();
            tree.i_fieldTransaction = (IndexTransaction)a_param;
            return tree;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    final Indexable4 handler(){
        return i_fieldTransaction.i_index._handler;
    }
    
    final Index4 index(){
        return i_fieldTransaction.i_index;
    }
    
    /**
     * Overridden in IxFileRange
     * Only call directly after compare() 
     */
    int[] lowerAndUpperMatch(){
        return null;
    }
    
    public final int nodes(){
        return _nodes;
    }
    
    public final void nodes(int count){
       _nodes = count;
    }
    
    public void setSizeOwn(){
        super.setSizeOwn();
        _nodes = 1;
    }
    
    public void setSizeOwnPrecedingSubsequent(){
        super.setSizeOwnPrecedingSubsequent();
        _nodes = 1 + i_preceding.nodes() + i_subsequent.nodes();
    }
    
    public void setSizeOwnPreceding(){
        super.setSizeOwnPreceding();
        _nodes = 1 + i_preceding.nodes();
    }
    
    public void setSizeOwnSubsequent(){
        super.setSizeOwnSubsequent();
        _nodes = 1 + i_subsequent.nodes();
    }
    
    public final void setSizeOwnPlus(Tree tree){
        super.setSizeOwnPlus(tree);
        _nodes = 1 + tree.nodes();
    }
    
    public final void setSizeOwnPlus(Tree tree1, Tree tree2){
        super.setSizeOwnPlus(tree1, tree2);
        _nodes = 1 + tree1.nodes() + tree2.nodes();
    }
    
    int slotLength(){
        return handler().linkLength() + YapConst.YAPINT_LENGTH;
    }
    
    final YapFile stream(){
        return trans().i_file;
    }
    
    final Transaction trans(){
        return i_fieldTransaction.i_trans;
    }
    
    public abstract void visit(Object obj);
    
    public abstract void visit(Visitor4 visitor, int[] a_lowerAndUpperMatch);
    
    public abstract void visitAll(IntObjectVisitor visitor);
    
    public abstract void freespaceVisit(FreespaceVisitor visitor, int index);
    
    public abstract int write(Indexable4 a_handler, YapWriter a_writer);
    
    public void visitFirst(FreespaceVisitor visitor){
        if(i_preceding != null){
            ((IxTree)i_preceding).visitFirst(visitor);
            if(visitor.visited()){
                return;
            }
        }
        freespaceVisit(visitor, 0);
        if(visitor.visited()){
            return;
        }
        if(i_subsequent != null){
            ((IxTree)i_subsequent).visitFirst(visitor);
            if(visitor.visited()){
                return;
            }
        }
    }
    
    public void visitLast(FreespaceVisitor visitor){
        if(i_subsequent != null){
            ((IxTree)i_subsequent).visitLast(visitor);
            if(visitor.visited()){
                return;
            }
        }
        freespaceVisit(visitor, 0);
        if(visitor.visited()){
            return;
        }
        if(i_preceding != null){
            ((IxTree)i_preceding).visitLast(visitor);
            if(visitor.visited()){
                return;
            }
        }
    }
    

}

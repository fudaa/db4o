/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.Coercion4;
import com.db4o.reflect.*;


final class YShort extends YapJavaClass
{
    static final int LENGTH = YapConst.SHORT_BYTES + YapConst.ADDED_LENGTH;
	
	private static final Short i_primitive = new Short((short)0);
	
    public YShort(YapStream stream) {
        super(stream);
    }
    
    public Object coerce(ReflectClass claxx, Object obj) {
    	return Coercion4.toShort(obj);
    }
    public Object defaultValue(){
		return i_primitive;
	}
	
	public int getID(){
		return 8;
	}
	
	public int linkLength(){
		return LENGTH;
	}
	
	protected Class primitiveJavaClass(){
		return short.class;
	}
	
	Object primitiveNull(){
		return i_primitive;
	}
	
	Object read1(YapReader a_bytes){
		short ret = readShort(a_bytes);
		if(! Deploy.csharp){
			if(ret == Short.MAX_VALUE){
				return null;
			}
		}
		return new Short(ret);
	}
	
	static final short readShort(YapReader a_bytes){
		int ret = 0;
		if (Deploy.debug){
			a_bytes.readBegin(YapConst.YAPSHORT);
		}
		for (int i = 0; i < YapConst.SHORT_BYTES; i++){
			ret = (ret << 8) + (a_bytes._buffer[a_bytes._offset++] & 0xff);
		}
		if (Deploy.debug){
			a_bytes.readEnd();
		}
		return (short)ret;
	}

	public void write(Object a_object, YapWriter a_bytes){
		if (! Deploy.csharp && a_object == null){
			writeShort(Short.MAX_VALUE,a_bytes);
		} else {
			writeShort(((Short)a_object).shortValue(), a_bytes);
		}
	}
	
	static final void writeShort(int a_short, YapWriter a_bytes){
		if(Deploy.debug){
			a_bytes.writeBegin(YapConst.YAPSHORT);
		}
		for (int i = 0; i < YapConst.SHORT_BYTES; i++){
			a_bytes._buffer[a_bytes._offset++] = (byte) (a_short >> ((YapConst.SHORT_BYTES - 1 - i) * 8));
		}
		if(Deploy.debug){
			a_bytes.writeEnd();
		}
	}
	
	// Comparison_______________________
	
	private short i_compareTo;
	
	private short val(Object obj){
		return ((Short)obj).shortValue();
	}
	
	void prepareComparison1(Object obj){
		i_compareTo = val(obj);
	}
	
	boolean isEqual1(Object obj){
		return obj instanceof Short && val(obj) == i_compareTo;
	}
	
	boolean isGreater1(Object obj){
		return obj instanceof Short && val(obj) > i_compareTo;
	}
	
	boolean isSmaller1(Object obj){
		return obj instanceof Short && val(obj) < i_compareTo;
	}
	
	
}

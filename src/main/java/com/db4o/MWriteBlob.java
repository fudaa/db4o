/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import java.io.*;

import com.db4o.ext.*;
import com.db4o.foundation.network.*;


class MWriteBlob extends MsgBlob {

    void processClient(YapSocket sock) {
        Msg message = Msg.readMessage(getTransaction(), sock);
        if (message.equals(Msg.OK)) {
            try {
                i_currentByte = 0;
                i_length = this.i_blob.getLength();
                i_blob.getStatusFrom(this);
                i_blob.setStatus(Status.PROCESSING);
                FileInputStream inBlob = this.i_blob.getClientInputStream();
                copy(inBlob,sock,true);
                sock.flush();
                YapStream stream = getStream();
                message = Msg.readMessage(getTransaction(), sock);
                if (message.equals(Msg.OK)) {

                    // make sure to load the filename to i_blob
                    // to allow client databasefile switching
                    stream.deactivate(i_blob, Integer.MAX_VALUE);
                    stream.activate(i_blob, Integer.MAX_VALUE);

                    this.i_blob.setStatus(Status.COMPLETED);
                } else {
                    this.i_blob.setStatus(Status.ERROR);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    boolean processMessageAtServer(YapSocket sock) {
        try {
            YapStream stream = getStream();
            BlobImpl blobImpl = this.serverGetBlobImpl();
            if (blobImpl != null) {
                blobImpl.setTrans(getTransaction());
                File file = blobImpl.serverFile(null, true);
                Msg.OK.write(stream, sock);
                FileOutputStream fout = new FileOutputStream(file);
                copy(sock,fout,blobImpl.getLength(),false);
                Msg.OK.write(stream, sock);
            }
        } catch (Exception e) {
        }
        return true;
    }
}
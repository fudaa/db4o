/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.*;

/**
 * 
 */
class YapReferences implements Runnable {
    
    final Object            _queue;
    private final YapStream _stream;
    private SimpleTimer     _timer;
    public final boolean    _weak;

    YapReferences(YapStream a_stream) {
        _stream = a_stream;
        _weak = (!(a_stream instanceof YapObjectCarrier)
            && Platform4.hasWeakReferences() && a_stream.i_config.i_weakReferences);
        _queue = _weak ? Platform4.createReferenceQueue() : null;
    }

    Object createYapRef(YapObject a_yo, Object obj) {
        
        if (!_weak) {  
            return obj;
        }
        
        return Platform4.createYapRef(_queue, a_yo, obj);
    }

    void pollReferenceQueue() {
        if (_weak) { 
            Platform4.pollReferenceQueue(_stream, _queue);
        }
    }

    public void run() {
        pollReferenceQueue();
    }

    void startTimer() {
    	if (!_weak) {
    		return;
    	}
        
        if(! _stream.i_config.i_weakReferences){
            return;
        }
    	
        if (_stream.i_config.i_weakReferenceCollectionInterval <= 0) {
        	return;
        }

        if (_timer != null) {
        	return;
        }
        
        _timer = new SimpleTimer(this, _stream.i_config.i_weakReferenceCollectionInterval, "db4o WeakReference collector");
    }

    void stopTimer() {
    	if (_timer == null){
            return;
        }
        _timer.stop();
        _timer = null;
    }
    
}
/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.Coercion4;
import com.db4o.reflect.*;


final class YByte extends YapJavaClass
{

    static final int LENGTH = 1 + YapConst.ADDED_LENGTH;
	
	private static final Byte i_primitive = new Byte((byte)0);
	
    public YByte(YapStream stream) {
        super(stream);
    }
    
    public Object coerce(ReflectClass claxx, Object obj) {
    	return Coercion4.toSByte(obj);
    }

	public int getID(){
		return 6;
	}
	
	public Object defaultValue(){
		return i_primitive;
	}
	
	boolean isNoConstraint(Object obj, boolean isPrimitive){
		return obj.equals(primitiveNull());
	}
	
	public int linkLength(){
		return LENGTH;
	}

	protected Class primitiveJavaClass(){
		return byte.class;
	}
	
	Object primitiveNull(){
		return i_primitive;
	}
	
	Object read1(YapReader a_bytes){
		if (Deploy.debug){
			a_bytes.readBegin(YapConst.YAPBYTE);
		}
		byte ret = a_bytes.readByte();
		if (Deploy.debug){
			a_bytes.readEnd();
		}
		return new Byte(ret);
	}
	
	public void write(Object a_object, YapWriter a_bytes){
		if(Deploy.debug){
			a_bytes.writeBegin(YapConst.YAPBYTE);
		}
		byte set;
		if (a_object == null){
			set = (byte)0;
		} else {
			set = ((Byte)a_object).byteValue();
		}
		a_bytes.append(set);
		if(Deploy.debug){
			a_bytes.writeEnd();
		}
	}
	
	public boolean readArray(Object array, YapWriter reader) {
        if(array instanceof byte[]){
            reader.readBytes((byte[])array);
            return true;
        }
        
        return false;
	}

    public boolean writeArray(Object array, YapWriter writer) {
        if(array instanceof byte[]){
            writer.append((byte[])array);
            return true;
        }
        return false;
    }   
    

					
	// Comparison_______________________
	
	private byte i_compareTo;
	
	private byte val(Object obj){
		return ((Byte)obj).byteValue();
	}
	
	void prepareComparison1(Object obj){
		i_compareTo = val(obj);
	}
	
	boolean isEqual1(Object obj){
		return obj instanceof Byte && val(obj) == i_compareTo;
	}
	
	boolean isGreater1(Object obj){
		return obj instanceof Byte && val(obj) > i_compareTo;
	}
	
	boolean isSmaller1(Object obj){
		return obj instanceof Byte && val(obj) < i_compareTo;
	}
}

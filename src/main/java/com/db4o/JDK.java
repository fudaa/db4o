/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import java.io.*;
import java.lang.reflect.*;
import java.util.*;

import com.db4o.config.*;
import com.db4o.foundation.*;
import com.db4o.reflect.*;
import com.db4o.reflect.generic.*;
import com.db4o.types.*;

/**
 */
public class JDK {
	
	Thread addShutdownHook(Runnable a_runnable){
		return null;
	}
	
	Db4oCollections collections(YapStream a_stream){
	    return null;
	}
    
    Class constructorClass(){
        return null;
    }
	
	Object createReferenceQueue() {
		return null;
	}

	Object createYapRef(Object a_queue, YapObject a_yapObject, Object a_object) {
		return null;
	}

	void forEachCollectionElement(Object a_object, Visitor4 a_visitor) {
        if(! Deploy.csharp){
            Enumeration e = null;
            if (a_object instanceof Hashtable) {
                e = ((Hashtable)a_object).elements();
            } else if (a_object instanceof Vector) {
                e = ((Vector)a_object).elements();
            }
            if (e != null) {
                while (e.hasMoreElements()) {
                    a_visitor.visit(e.nextElement());
                }
            }
        }
	}
	
	ClassLoader getContextClassLoader(){
		return null;
	}

	Object getYapRefObject(Object a_object) {
		return null;
	}
    
    boolean isCollectionTranslator(Config4Class a_config) {
        if(!Deploy.csharp){
            if (a_config != null) {
                ObjectTranslator ot = a_config.getTranslator();
                if (ot != null) {
                    return ot instanceof THashtable;
                }
            }
        }
        return false;
    }

	public int ver(){
	    return 1;
	}
	
	void killYapRef(Object obj){
		
	}
	
	synchronized void lock(RandomAccessFile file) {
	}
	
    /**
     * use for system classes only, since not ClassLoader
     * or Reflector-aware
     */
	boolean methodIsAvailable(
            String className,
            String methodName,
            Class[] params) {
    	return false;
    }

	void pollReferenceQueue(YapStream a_stream, Object a_referenceQueue) {
	}
	
	public void registerCollections(GenericReflector reflector) {
		
	}
	
	void removeShutdownHook(Thread a_thread){
		
	}
	
	public Constructor serializableConstructor(Class clazz){
	    return null;
	}

	void setAccessible(Object a_accessible) {
	}
    
    boolean isEnum(Reflector reflector, ReflectClass clazz) {
        return false;
    }
	
	synchronized void unlock(RandomAccessFile file) {
	}
    
}

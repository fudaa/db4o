/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import java.io.*;

import com.db4o.config.*;
import com.db4o.ext.*;
import com.db4o.foundation.*;
import com.db4o.foundation.network.*;
import com.db4o.inside.*;

class YapServer implements ObjectServer, ExtObjectServer, Runnable, YapSocketFakeServer {
    private String i_name;

    private YapServerSocket i_serverSocket;

    private int i_threadIDGen = 1;
    private Collection4 i_threads = new Collection4();
    private YapFile i_yapFile;

    YapServer(final YapFile a_yapFile, int a_port) {
        a_yapFile.setServer(true);
        i_name = "db4o ServerSocket  FILE: " + a_yapFile.toString() + "  PORT:" + a_port;
        i_yapFile = a_yapFile;
        Config4Impl config = (Config4Impl) i_yapFile.configure();
        config.callbacks(false);
        config.i_isServer = true;
        
        a_yapFile.getYapClass(a_yapFile.i_handlers.ICLASS_STATICCLASS, true);
        
        // make sure all configured YapClasses are up in the repository
        config.i_exceptionalClasses.forEachValue(new Visitor4() {
            public void visit(Object a_object) {
                a_yapFile.getYapClass(a_yapFile.reflector().forName(((Config4Class)a_object).getName()), true);
            }
        });
        
        if (config.i_messageLevel == 0) {
            config.i_messageLevel = 1;
        }
        if (a_port > 0) {
            try {
                i_serverSocket = new YapServerSocket(a_port);
                i_serverSocket.setSoTimeout(config.i_timeoutServerSocket);
            } catch (IOException e) {
                Exceptions4.throwRuntimeException(30, "" + a_port);
            }
            
            new Thread(this).start(); 
            // TODO: Carl, shouldn't this be a daemon?
            // Not sure Klaus, let's discuss. 
            
            synchronized (this) {
                try {
                    wait(1000);
                    // Give the thread some time to get up.
                    // We will get notified.
                } catch (Exception e) {
                }
            }
        }
    }
    
    public void backup(String path) throws IOException{
        i_yapFile.backup(path);
    }

    final void checkClosed() {
        if (i_yapFile == null) {
            Exceptions4.throwRuntimeException(20, i_name);
        }
        i_yapFile.checkClosed();
    }

    public boolean close() {
        synchronized (Db4o.lock) {
            // Take it easy. 
            // Test cases hit close while communication
            // is still in progress.
        	Cool.sleepIgnoringInterruption(100);
            try {
            	if(i_serverSocket != null){
					i_serverSocket.close();
            	}
            } catch (Exception e) {
                if (Deploy.debug) {
                    System.out.println("YapServer.close() ServerSocket failed to close.");
                }
            }
            i_serverSocket = null;
            boolean isClosed = i_yapFile == null ? true : i_yapFile.close();
            synchronized (i_threads) {
                Iterator4 i = i_threads.iterator();
                while (i.hasNext()) {
                    ((YapServerThread) i.next()).close();
                }
            }
            i_yapFile = null;
            return isClosed;
        }
    }

    public Configuration configure() {
        return i_yapFile.configure();
    }

    public ExtObjectServer ext() {
        return this;
    }

    YapServerThread findThread(int a_threadID) {
        synchronized (i_threads) {
            Iterator4 i = i_threads.iterator();
            while (i.hasNext()) {
                YapServerThread serverThread = (YapServerThread) i.next();
                if (serverThread.i_threadID == a_threadID) {
                    return serverThread;
                }
            }
        }
        return null;
    }

    public void grantAccess(String userName, String password) {
        synchronized (i_yapFile.i_lock) {
            checkClosed();
            User user = new User();
            user.name = userName;
            i_yapFile.showInternalClasses(true);
            User existing = (User) i_yapFile.get(user).next();
            if (existing != null) {
                existing.password = password;
                i_yapFile.set(existing);
            } else {
                user.password = password;
                i_yapFile.set(user);
            }
            i_yapFile.commit();
            i_yapFile.showInternalClasses(false);
        }
    }

    public ObjectContainer objectContainer() {
        return i_yapFile;
    }

    public ObjectContainer openClient() {
        try {
            return new YapClient(
                openClientSocket(),
                YapConst.EMBEDDED_CLIENT_USER + (i_threadIDGen - 1),
                "",
                false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public YapSocketFake openClientSocket() {
    	int timeout = ((Config4Impl)configure()).i_timeoutClientSocket;
        YapSocketFake clientFake = new YapSocketFake(this, timeout);
        YapSocketFake serverFake = new YapSocketFake(this, timeout, clientFake);
        try {
            YapServerThread thread =
                new YapServerThread(this, i_yapFile, serverFake, i_threadIDGen++, true);
            synchronized (i_threads) {
                i_threads.add(thread);
            }
            thread.start();
            return clientFake;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    void removeThread(YapServerThread aThread) {
        synchronized (i_threads) {
            i_threads.remove(aThread);
        }
    }

    public void revokeAccess(String userName) {
        synchronized (i_yapFile.i_lock) {
            i_yapFile.showInternalClasses(true);
            checkClosed();
            User user = new User();
            user.name = userName;
            ObjectSet set = i_yapFile.get(user);
            while (set.hasNext()) {
                i_yapFile.delete(set.next());
            }
            i_yapFile.commit();
            i_yapFile.showInternalClasses(false);
        }
    }

    public void run() {
        Thread.currentThread().setName(i_name);
        i_yapFile.logMsg(31, "" + i_serverSocket.getLocalPort());
        synchronized (this) {
            this.notify();
        }
        while (i_serverSocket != null) {
            try {
                YapServerThread thread =
                    new YapServerThread(
                        this,
                        i_yapFile,
                        i_serverSocket.accept(),
                        i_threadIDGen++,
                        false);
                synchronized (i_threads) {
                    i_threads.add(thread);
                }
                thread.start();
            } catch (Exception e) {
            }
        }
    }
}

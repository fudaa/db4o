/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import java.lang.reflect.*;

import com.db4o.reflect.generic.*;

/**
 * 
 * package and class name are hard-referenced in JavaOnly#jdk()
 * 
 * TODO: may need to use this on instead of JDK on .NET. Check!
 * 
 */
class JDKReflect extends JDK {
    
    Class constructorClass(){
        return Constructor.class;
    }
	
    /**
     * use for system classes only, since not ClassLoader
     * or Reflector-aware
     */
    final boolean methodIsAvailable(
        String className,
        String methodName,
        Class[] params) {
    	
        try {
        	
            Class clazz = Class.forName(className);
            if (clazz.getMethod(methodName, params) !=null) {
                return true;
            }
            return false;
        } catch (Throwable t) {
        }
        return false;
    }
    
    /**
     * use for system classes only, since not ClassLoader
     * or Reflector-aware
     */
    protected Object invoke (Object obj, String methodName, Class[] paramClasses, Object[] params){
        return invoke(obj.getClass().getName(), methodName, paramClasses, params, obj );
    }
    
    /**
     * use for system classes only, since not ClassLoader
     * or Reflector-aware
     */
    protected Object invoke (String className, String methodName, Class[] paramClasses, Object[] params, Object onObject){
        try {
                Method method = getMethod(className, methodName, paramClasses);
                return method.invoke(onObject, params);
            } catch (Throwable t) {
            }
        return null;
    }

    /**
     * calling this "method" will break C# conversion with the old converter
     * 
     * use for system classes only, since not ClassLoader
     * or Reflector-aware
     */
    protected Method getMethod(String className, String methodName, Class[] paramClasses) {
        try {
            Class clazz = Class.forName(className);
            Method method = clazz.getMethod(methodName, paramClasses);
            return method;
        } catch (Throwable t) {
        }
        return null;
    }
    
    public void registerCollections(GenericReflector reflector) {
        if(! Deploy.csharp){
            reflector.registerCollection(java.util.Vector.class);
            reflector.registerCollection(java.util.Hashtable.class);
            reflector.registerCollectionUpdateDepth(java.util.Hashtable.class, 3);
        }
    }

}

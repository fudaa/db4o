/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.*;

/**
 */
public abstract class YapMeta {
    
    int i_id = 0; // UID and address of pointer to the object in our file

    protected int i_state = 2; // DIRTY and ACTIVE

    final boolean beginProcessing() {
        if (bitIsTrue(YapConst.PROCESSING)) {
            return false;
        }
        bitTrue(YapConst.PROCESSING);
        return true;
    }

    final void bitFalse(int bitPos) {
        i_state &= ~(1 << bitPos);
    }
    
    final boolean bitIsFalse(int bitPos) {
        return (i_state | (1 << bitPos)) != i_state;
    }

    final boolean bitIsTrue(int bitPos) {
        return (i_state | (1 << bitPos)) == i_state;
    }

    final void bitTrue(int bitPos) {
        i_state |= (1 << bitPos);
    }

    void cacheDirty(Collection4 col) {
        if (!bitIsTrue(YapConst.CACHED_DIRTY)) {
            bitTrue(YapConst.CACHED_DIRTY);
            col.add(this);
        }
    }

    void endProcessing() {
        bitFalse(YapConst.PROCESSING);
    }

    public int getID() {
        return i_id;
    }

    abstract byte getIdentifier();

    public final boolean isActive() {
        return bitIsTrue(YapConst.ACTIVE);
    }

    public boolean isDirty() {
        return bitIsTrue(YapConst.ACTIVE) && (!bitIsTrue(YapConst.CLEAN));
    }

    public int linkLength() {
        return YapConst.YAPID_LENGTH;
    }

    final void notCachedDirty() {
        bitFalse(YapConst.CACHED_DIRTY);
    }

    abstract int ownLength();

    void read(Transaction a_trans) {
        try {
            if (beginProcessing()) {
                YapReader reader = a_trans.i_stream.readReaderByID(a_trans, getID());
                if (reader != null) {
                    if (Deploy.debug) {
                        reader.readBegin(getID(), getIdentifier());
                    }
                    readThis(a_trans, reader);
                    setStateOnRead(reader);
                }
                endProcessing();
            }
        } catch (LongJumpOutException ljoe) {
            throw ljoe;
        } catch (Throwable t) {
            if (Debug.atHome) {
                t.printStackTrace();
            }
        }
    }
    
    abstract void readThis(Transaction a_trans, YapReader a_reader);


    void setID(YapStream a_stream, int a_id) {
        i_id = a_id;
    }

    final void setStateClean() {
        bitTrue(YapConst.ACTIVE);
        bitTrue(YapConst.CLEAN);
    }

    final void setStateDeactivated() {
        bitFalse(YapConst.ACTIVE);
    }

    void setStateDirty() {
        bitTrue(YapConst.ACTIVE);
        bitFalse(YapConst.CLEAN);
    }

    void setStateOnRead(YapReader reader) {
        if (Deploy.debug) {
            reader.readEnd();
        }
        if (bitIsTrue(YapConst.CACHED_DIRTY)) {
            setStateDirty();
        } else {
            setStateClean();
        }
    }

    final YapWriter write(Transaction a_trans) {
        if (writeObjectBegin()) {
            
            YapFile stream = (YapFile)a_trans.i_stream;

            YapWriter writer =
                (getID() == 0)
                    ? stream.newObject(a_trans, this)
                    : stream.updateObject(a_trans, this);

            writeThis(writer);

            if (Deploy.debug) {
                writer.writeEnd();
                writer.debugCheckBytes();
            }

            ((YapFile)stream).writeObject(this, writer);

            if (isActive()) {
                setStateClean();
            }
            endProcessing();


            return writer;
        }
        return null;
    }

    boolean writeObjectBegin() {
        if (isDirty()) {
            return beginProcessing();
        }
        return false;
    }

    void writeOwnID(YapWriter a_writer) {
        write(a_writer.getTransaction());
        a_writer.writeInt(getID());
    }

    abstract void writeThis(YapWriter a_writer);

    static final void writeIDOf(YapMeta a_object, YapWriter a_writer) {
        if (a_object != null) {
            a_object.writeOwnID(a_writer);
        } else {
            a_writer.writeInt(0);
        }
    }
}

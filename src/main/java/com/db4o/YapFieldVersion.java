/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.ext.*;

/**
 * 
 */
class YapFieldVersion extends YapFieldVirtual {

    YapFieldVersion(YapStream stream) {
        super();
        i_name = VirtualField.VERSION;
        i_handler = new YLong(stream);
    }
    
    void addFieldIndex(YapWriter a_writer, boolean a_new) {
        YLong.writeLong(a_writer.getStream().bootRecord().version(), a_writer);
    }
    
    void delete(YapWriter a_bytes, boolean isUpdate) {
        a_bytes.incrementOffset(linkLength());
    }

    void instantiate1(Transaction a_trans, YapObject a_yapObject, YapReader a_bytes) {
        a_yapObject.i_virtualAttributes.i_version = YLong.readLong(a_bytes);
    }

    void marshall1(YapObject a_yapObject, YapWriter a_bytes, boolean a_migrating, boolean a_new) {
        YapStream stream = a_bytes.getStream().i_parent;
        PBootRecord br = stream.bootRecord();
        if (!a_migrating) {
            if (br != null) {
                a_yapObject.i_virtualAttributes.i_version = br.version();
            }
        }else{
            if(br != null){
                br.setDirty();
            }
        }
        if(a_yapObject.i_virtualAttributes == null){
            YLong.writeLong(0, a_bytes);
        }else{
            YLong.writeLong(a_yapObject.i_virtualAttributes.i_version, a_bytes);
        }
    }

    public int linkLength() {
        return YapConst.YAPLONG_LENGTH;
    }
    
    void marshallIgnore(YapWriter writer) {
        YLong.writeLong(0, writer);
    }


}
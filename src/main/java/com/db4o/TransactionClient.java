/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

import com.db4o.foundation.*;

final class TransactionClient extends Transaction {

    private final YapClient i_client;
    private Tree i_yapObjectsToGc; 
    

    TransactionClient(YapClient a_stream, Transaction a_parent) {
        super(a_stream, a_parent);
        i_client = a_stream;
    }

    void beginEndSet() {
        if (i_delete != null) {
            i_delete.traverse(new Visitor4() {
                public void visit(Object a_object) {
                    DeleteInfo info = (DeleteInfo) a_object;
                    if (info._delete && info._reference != null) {
                        i_yapObjectsToGc = Tree.add(i_yapObjectsToGc, new TreeIntObject(info.i_key, info._reference));
                    }
                }
            });
        }
        i_delete = null;
        i_writtenUpdateDeletedMembers = null;
        i_client.writeMsg(Msg.TA_BEGIN_END_SET);
    }

    void commit() {
        commitTransactionListeners();
        if(i_yapObjectsToGc != null){
            i_yapObjectsToGc.traverse(new Visitor4() {
                public void visit(Object a_object) {
                    YapObject yo = (YapObject)((TreeIntObject) a_object).i_object;
                    i_stream.yapObjectGCd(yo);
                }
            });
        }
        i_yapObjectsToGc = null;
        i_client.writeMsg(Msg.COMMIT);
    }

    void delete(YapObject a_yo, int a_cascade) {
        super.delete(a_yo, a_cascade);
        i_client.writeMsg(Msg.TA_DELETE.getWriterForInts(this, new int[] {a_yo.getID(), a_cascade}));
    }

    void dontDelete(int classID, int a_id) {
        super.dontDelete(classID, a_id);
        i_client.writeMsg(Msg.TA_DONT_DELETE.getWriterForInts(this, new int[]{classID, a_id}));
    }

    boolean isDeleted(int a_id) {

        // This one really is a hack.
        // It only helps to get information about the current
        // transaction.

        // We need a better strategy for C/S concurrency behaviour.
        
        i_client.writeMsg(Msg.TA_IS_DELETED.getWriterForInt(this, a_id));
        int res = i_client.expectedByteResponse(Msg.TA_IS_DELETED).readInt();
        return res == 1;
    }
    
    Object[] objectAndYapObjectBySignature(final long a_uuid, final byte[] a_signature) {
        int messageLength = YapConst.YAPLONG_LENGTH + YapConst.YAPINT_LENGTH + a_signature.length;
        MsgD message = Msg.OBJECT_BY_UUID.getWriterForLength(this, messageLength);
        message.writeLong(a_uuid);
        message.writeBytes(a_signature);
        i_client.writeMsg(message);
        message = (MsgD)i_client.expectedResponse(Msg.OBJECT_BY_UUID);
        int id = message.readInt();
        if(id > 0){
            return i_stream.getObjectAndYapObjectByID(this, id);
        }
        return new Object[2];
    }
    
    public void rollback() {
        i_yapObjectsToGc = null;
        rollBackTransactionListeners();
    }

    void writeUpdateDeleteMembers(int a_id, YapClass a_yc, int a_type,
        int a_cascade) {
        i_client.writeMsg(Msg.WRITE_UPDATE_DELETE_MEMBERS.getWriterForInts(this,
            new int[]{
            a_id,
            a_yc.getID(),
            a_type,
            a_cascade
        }));
    }
}
/* Copyright (C) 2004 - 2005  db4objects Inc.  http://www.db4o.com

This file is part of the db4o open source object database.

db4o is free software; you can redistribute it and/or modify it under
the terms of version 2 of the GNU General Public License as published
by the Free Software Foundation and as clarified by db4objects' GPL 
interpretation policy, available at
http://www.db4o.com/about/company/legalpolicies/gplinterpretation/
Alternatively you can write to db4objects, Inc., 1900 S Norfolk Street,
Suite 350, San Mateo, CA 94403, USA.

db4o is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
package com.db4o;

/**
 */
class QResultClient extends QueryResultImpl {

	private Object[] i_prefetched = new Object[YapConst.PREFETCH_OBJECT_COUNT];
	private int i_remainingObjects;
	private int i_prefetchCount = YapConst.PREFETCH_OBJECT_COUNT;
	private int i_prefetchRight;

	QResultClient(Transaction a_ta) {
		super(a_ta);
	}
    
    QResultClient(Transaction a_ta, int initialSize) {
        super(a_ta, initialSize);
    }

	
	public boolean hasNext() {
		synchronized (streamLock()) {
			if(i_remainingObjects > 0){
				return true;
			}
			return super.hasNext();
		}
	}
	

	public Object next() {
		synchronized (streamLock()) {
			YapClient stream = (YapClient)i_trans.i_stream;
			stream.checkClosed();
			if (i_remainingObjects < 1) {
				if (super.hasNext()) {
					i_remainingObjects = (stream).prefetchObjects(this, i_prefetched, i_prefetchCount);
					i_prefetchRight=i_remainingObjects;
				}
			}
			i_remainingObjects --;
			if(i_remainingObjects < 0){
				return null;
			}
			if(i_prefetched[i_prefetchRight-i_remainingObjects-1] == null){
				return next();
			}
			return activate(i_prefetched[i_prefetchRight-i_remainingObjects-1]);
		}
	}
	
	public void reset() {
		synchronized (streamLock()) {
			i_remainingObjects = 0;
			i_prefetchRight=0;
			super.reset();
		}
	}
	
	// TODO: open this as an external tuning interface in ExtObjectSet
	
//	public void prefetch(int count){
//		if(count < 1){
//			count = 1;
//		}
//		i_prefetchCount = count;
//		Object[] temp = new Object[i_prefetchCount];
//		if(i_remainingObjects > 0){
//			// Potential problem here: 
//			// On reducing the prefetch size, this will crash.
//			System.arraycopy(i_prefetched, 0, temp, 0, i_remainingObjects);
//		}
//		i_prefetched = temp;
//	}


}
